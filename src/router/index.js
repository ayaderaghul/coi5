import Vue from 'vue'
import Router from 'vue-router'
import StartingPage from '@/components/StartingPage'
import EntryPoint from '@/components/EntryPoint'
import LoginPage from '@/components/LoginPage'
import Profile from '@/components/Profile'

import All from '@/components/All'
import Account from '@/components/Account'
import Invest from '@/components/Invest'
import History from '@/components/History'
import Utility from '@/components/Utility'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
	{
	    path: '/',
	    name: 'StartingPage',
	    component: StartingPage
	},
	{
	    path: '/entry',
	    name: 'EntryPoint',
	    component: EntryPoint
	},
	{
	    path: '/login',
	    name: 'LoginPage',
	    component: LoginPage,
	},
	{
	    path: '/profile',
	    name: 'Profile',
	    component: Profile,
	    children: [
		{
		    path: 'all',
		    name: 'All',
		    component: All
		},
		{
		    path: 'account',
		    name: 'Account',
		    component: Account
		},
		{
		    path: 'invest',
		    name: 'Invest',
		    component: Invest
		},
		{
		    path: 'history',
		    name: 'History',
		    component: History
		},
		{
		    path: 'utility',
		    name: 'Utility',
		    component: Utility
		}
	    ]
	}
    ],
    linkActiveClass: 'is-active'
    
})
